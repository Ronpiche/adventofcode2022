from dataclasses import dataclass


class Forest:

    FILENAME = "exo8/input.txt"

    def __init__(self):
        f = open(Forest.FILENAME, "r")
        self.m = [[s for s in line if s != "\n"] for line in f]
        self.m_flipped = [ list(e) for e in zip(*self.m)]
        n = 0
        for x in range(len(self.m)):
            for y in range(len(self.m[x])):
                n += int(self._v_north(x, y) or self._v_south(x, y) or self._v_east(x, y) or self._v_west(x, y))
        print(n)

    def _v_south(self, x, y):
        return True if x==len(self.m_flipped[y])-1 else max(self.m_flipped[y][x+1:]) < self.m[x][y]

    def _v_north(self, x, y):
        return True if x==0 else max(self.m_flipped[y][:x]) < self.m[x][y]

    def _v_east(self, x, y):
        return True if y==len(self.m[x])-1 else max(self.m[x][y+1:]) < self.m[x][y]

    def _v_west(self, x, y):
        return True if y==0 else max(self.m[x][:y]) < self.m[x][y]
